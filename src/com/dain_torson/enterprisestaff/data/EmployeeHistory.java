package com.dain_torson.enterprisestaff.data;

public class EmployeeHistory {

    private String post;
    private String startDate;
    private String endDate;

    private Integer department;
    private Integer level;

    public EmployeeHistory(Integer department, String post, Integer level, String startDate, String endDate) {
        this.post = post;
        this.startDate = startDate;
        this.endDate = endDate;
        this.department = department;
        this.level = level;
    }

    public String getPost() {
        return post;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public Integer getDepartment() {
        return department;
    }

    public Integer getLevel() {
        return level;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
