package com.dain_torson.enterprisestaff.data;

public class Post {

    private String name;
    private Integer lowerLevel;
    private Integer upperLevel;
    private Integer number;

    public Post(String name, Integer lowerLevel, Integer upperLevel, Integer number) {
        this.name = name;
        this.lowerLevel = lowerLevel;
        this.upperLevel = upperLevel;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public Integer getLowerLevel() {
        return lowerLevel;
    }

    public Integer getUpperLevel() {
        return upperLevel;
    }

    public Integer getNumber() {
        return number;
    }
}
