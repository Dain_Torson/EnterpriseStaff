package com.dain_torson.enterprisestaff.data;

public class Person {

    private String fio;
    private String gender;
    private String maritalStatus;
    private String post;
    

    private Integer department;
    private Integer age;

    public Person(String fio, String post, Integer department, Integer age, String gender, String maritalStatus) {
        this.fio = fio;
        this.gender = gender;
        this.maritalStatus = maritalStatus;
        this.post = post;
        this.department = department;
        this.age = age;
    }

    public String getFio() {
        return fio;
    }

    public String getGender() {
        return gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public Integer getAge() {
        return age;
    }

    public String getPost() {
        return post;
    }

    public Integer getDepartment() {
        return department;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
