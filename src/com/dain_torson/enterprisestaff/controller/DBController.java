package com.dain_torson.enterprisestaff.controller;


import com.dain_torson.enterprisestaff.controller.tasks.*;
import com.dain_torson.enterprisestaff.data.EmployeeHistory;
import com.dain_torson.enterprisestaff.data.Person;
import com.dain_torson.enterprisestaff.view.DBView;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.util.List;
import java.util.Properties;

public class DBController {

    private String url;

    private Properties props;
    private DBView output;

    public DBController(String url, String user, String password, DBView output) {
        this.url = url;
        this.output = output;

        props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", password);
    }

    public void updateSchedule(int departmentId) {

        UpdateScheduleTask task = new UpdateScheduleTask(url, props, departmentId, output);

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    public void updateElders() {

        UpdateEldersTask task = new UpdateEldersTask(url, props, output);

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    public void updateEmployees(int age, String post) {

        UpdateEmpOutputTask task = new UpdateEmpOutputTask(url, props, age, post, output);

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    public void getPerson(String name, List<TextField> textFields, List<ChoiceBox<String>> choiceBoxes) {

        FindPersonTask task = new FindPersonTask(url, props, name, textFields, choiceBoxes);

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    public void getHistory(String name, String date, List<TextField> textFields, DatePicker datePicker) {

        FindHistotyTask task = new FindHistotyTask(url, props, name, date, textFields, datePicker);

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    public void addEmployee(Person person) {

        AddEmployeeTask task = new AddEmployeeTask(url, props, person);

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    public void updateEmployee(Person person) {
        UpdateEmployeeTask task = new UpdateEmployeeTask(url, props, person);

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    public void deleteEmployee(String name) {
        DeleteEmployeeTask task = new DeleteEmployeeTask(url, props, name);

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    public void addHistory(String name, EmployeeHistory history) {

        AddHistoryTask task = new AddHistoryTask(url, props, name, history);

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    public void updateHistory(String name, EmployeeHistory history) {

        UpdateHistoryTask task = new UpdateHistoryTask(url, props, name, history);

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();

    }

    public void deleteHistory(String name, String date) {
        DeleteHistoryTask task = new DeleteHistoryTask(url, props, name, date);

        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }
}
