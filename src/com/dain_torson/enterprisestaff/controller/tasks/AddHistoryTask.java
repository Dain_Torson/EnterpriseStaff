package com.dain_torson.enterprisestaff.controller.tasks;

import com.dain_torson.enterprisestaff.data.EmployeeHistory;
import com.dain_torson.enterprisestaff.data.Person;
import javafx.scene.control.DatePicker;

import java.sql.*;
import java.util.Properties;

public class AddHistoryTask extends DBTask{

    private String name;
    private EmployeeHistory history;

    public AddHistoryTask(String url, Properties props, String name, EmployeeHistory history) {
        super(url, props);
        this.name = name;
        this.history = history;
    }

    @Override
    protected Void call() throws Exception {

        Connection conn = DriverManager.getConnection(url, props);

        PreparedStatement st = conn.prepareStatement("SELECT * FROM add_hist(?, ?, ?, ?, ?, ?)");
        st.setString(1, name);
        st.setInt(2, history.getDepartment());
        st.setString(3, history.getPost());
        st.setInt(4, history.getLevel());
        Date startDate = Date.valueOf(history.getStartDate());
        Date endDate = Date.valueOf(history.getEndDate());
        st.setDate(5, startDate);
        st.setDate(6, endDate);

        ResultSet rs = st.executeQuery();

        return null;
    }
}
