package com.dain_torson.enterprisestaff.controller.tasks;


import com.dain_torson.enterprisestaff.data.Person;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

import java.sql.*;
import java.util.List;
import java.util.Properties;

public class FindPersonTask extends DBTask{

    private String name;
    private Person person;
    private List<TextField> textFields;
    private List<ChoiceBox<String>> choiceBoxes;

    public FindPersonTask(String url, Properties props, String name, List<TextField> textFields,
                          List<ChoiceBox<String>> choiceBoxes) {
        super(url, props);
        this.name = name;
        this.textFields = textFields;
        this.choiceBoxes = choiceBoxes;
        person = new Person(name, "", 0, 0, "", "");
    }

    @Override
    protected Void call() throws Exception {

        Connection conn = DriverManager.getConnection(url, props);

        PreparedStatement st = conn.prepareStatement("SELECT * FROM employee WHERE fio = ?");
        st.setString(1, name);
        ResultSet rs = st.executeQuery();

        while (rs.next())
        {
            person.setPost(rs.getString(3));
            person.setDepartment(rs.getInt(4));
            person.setAge(rs.getInt(5));
            person.setGender(rs.getString(6));
            person.setMaritalStatus(rs.getString(7));
        }
        rs.close();
        st.close();

        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();

        textFields.get(0).setText(String.valueOf(person.getAge()));
        textFields.get(1).setText(person.getPost());
        textFields.get(2).setText(String.valueOf(person.getDepartment()));

        if(person.getGender().contains("муж")) {
            choiceBoxes.get(0).getSelectionModel().select(0);
        }
        else {
            choiceBoxes.get(0).getSelectionModel().select(1);
        }

        if(person.getMaritalStatus().contains("Холост") || person.getMaritalStatus().contains("Не замужем")) {
            choiceBoxes.get(1).getSelectionModel().select(0);
        }
        else {
            choiceBoxes.get(1).getSelectionModel().select(1);
        }
    }
}
