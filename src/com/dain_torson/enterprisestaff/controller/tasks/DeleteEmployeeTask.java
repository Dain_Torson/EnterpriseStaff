package com.dain_torson.enterprisestaff.controller.tasks;

import com.dain_torson.enterprisestaff.data.Person;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

public class DeleteEmployeeTask extends DBTask{
    private String name;

    public DeleteEmployeeTask(String url, Properties props, String name) {
        super(url, props);
        this.name = name;
    }

    @Override
    protected Void call() throws Exception {

        Connection conn = DriverManager.getConnection(url, props);

        PreparedStatement st = conn.prepareStatement("DELETE FROM employee WHERE fio = ?");
        st.setString(1, name);

        ResultSet rs = st.executeQuery();

        return null;
    }
}
