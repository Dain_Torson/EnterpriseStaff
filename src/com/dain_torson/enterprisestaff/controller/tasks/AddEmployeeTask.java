package com.dain_torson.enterprisestaff.controller.tasks;

import com.dain_torson.enterprisestaff.data.Person;

import java.sql.*;
import java.util.Properties;

/**
 * Created by Ales on 10.12.2015.
 */
public class AddEmployeeTask extends DBTask {

    private Person person;

    public AddEmployeeTask(String url, Properties props, Person person) {
        super(url, props);
        this.person = person;
    }

    @Override
    protected Void call() throws Exception {


        Connection conn = DriverManager.getConnection(url, props);


        PreparedStatement st = conn.prepareStatement("INSERT INTO employee" +
                " (fio, post_name, department, age, gender, status) VALUES" +
                "(?, ?, ?, ?, ?, ?);");
        st.setString(1, person.getFio());
        st.setString(2, person.getPost());
        st.setInt(3, person.getDepartment());
        st.setInt(4, person.getAge());
        st.setString(5, person.getGender());
        st.setString(6, person.getMaritalStatus());

        ResultSet rs = st.executeQuery();

        return null;
    }
}
