package com.dain_torson.enterprisestaff.controller.tasks;

import com.dain_torson.enterprisestaff.data.Person;
import com.dain_torson.enterprisestaff.data.Post;
import com.dain_torson.enterprisestaff.view.DBView;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class UpdateEldersTask extends DBTask{

    private DBView output;
    private List<Person> queryResult;

    public UpdateEldersTask(String url, Properties props, DBView output) {
        super(url, props);
        this.output = output;
        queryResult = new ArrayList<>();
    }

    @Override
    protected Void call() throws Exception {

        Connection conn = DriverManager.getConnection(url, props);

        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM get_elders()");

        while (rs.next())
        {
            Person person = new Person(rs.getString(2), rs.getString(3), rs.getInt(4),
                    rs.getInt(5), rs.getString(6), rs.getString(7));
            queryResult.add(person);
        }
        rs.close();
        st.close();

        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        output.setPersonData(queryResult);
    }
}
