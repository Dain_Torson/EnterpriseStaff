package com.dain_torson.enterprisestaff.controller.tasks;

import com.dain_torson.enterprisestaff.view.DBView;
import javafx.concurrent.Task;

import java.util.Properties;

public class DBTask extends Task<Void>{

    protected String url;
    protected Properties props;

    public DBTask(String url, Properties props) {
        this.url = url;
        this.props = props;
    }

    @Override
    protected Void call() throws Exception {
        return null;
    }
}
