package com.dain_torson.enterprisestaff.controller.tasks;

import com.dain_torson.enterprisestaff.data.Person;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

public class UpdateEmployeeTask extends DBTask{

    private Person person;

    public UpdateEmployeeTask(String url, Properties props, Person person) {
        super(url, props);
        this.person = person;
    }

    @Override
    protected Void call() throws Exception {

        Connection conn = DriverManager.getConnection(url, props);

        PreparedStatement st = conn.prepareStatement("UPDATE employee SET post_name = ?, department = ?, age = ?, " +
                "gender = ?, status = ? WHERE fio = ?");
        st.setString(6, person.getFio());
        st.setString(1, person.getPost());
        st.setInt(2, person.getDepartment());
        st.setInt(3, person.getAge());
        st.setString(4, person.getGender());
        st.setString(5, person.getMaritalStatus());

        ResultSet rs = st.executeQuery();

        return null;
    }
}
