package com.dain_torson.enterprisestaff.controller.tasks;

import com.dain_torson.enterprisestaff.controller.DBDateConverter;
import com.dain_torson.enterprisestaff.data.EmployeeHistory;
import com.dain_torson.enterprisestaff.data.Person;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.sql.*;
import java.util.List;
import java.util.Properties;

public class FindHistotyTask extends DBTask{

    private String name;
    private String date;
    private EmployeeHistory history;
    private List<TextField> textFields;
    private DatePicker datePicker;

    public FindHistotyTask(String url, Properties props, String name, String date,
                           List<TextField> textFields, DatePicker datePicker) {
        super(url, props);
        this.name = name;
        this.date = date;
        this.textFields = textFields;
        this.datePicker = datePicker;
        history = new EmployeeHistory(0, "", 0, "", "");
    }

    @Override
    protected Void call() throws Exception {

        Connection conn = DriverManager.getConnection(url, props);

        Date sqlDate = Date.valueOf(date);

        PreparedStatement st = conn.prepareStatement("SELECT * FROM get_hist(?, ?)");
        st.setString(1, name);
        st.setDate(2, sqlDate);
        ResultSet rs = st.executeQuery();

        while (rs.next())
        {
            history.setDepartment(rs.getInt(1));
            history.setPost(rs.getString(2));
            history.setLevel(rs.getInt(3));
            history.setStartDate(rs.getDate(4).toString());
            history.setEndDate(rs.getDate(5).toString());
        }
        rs.close();
        st.close();

        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();

        textFields.get(0).setText(String.valueOf(history.getDepartment()));
        textFields.get(1).setText(history.getPost());
        textFields.get(2).setText(String.valueOf(history.getLevel()));

        DBDateConverter converter = new DBDateConverter();
        datePicker.setValue(converter.fromString(history.getEndDate()));
    }
}
