package com.dain_torson.enterprisestaff.controller.tasks;

import com.dain_torson.enterprisestaff.data.Post;
import com.dain_torson.enterprisestaff.view.DBView;
import javafx.geometry.Pos;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class UpdateScheduleTask extends DBTask {

    private DBView output;
    private List<Post> queryResult;
    private int department;

    public UpdateScheduleTask(String url, Properties props, int department, DBView output) {
        super(url, props);
        this.output = output;
        this.department = department;
        queryResult = new ArrayList<>();
    }

    @Override
    protected Void call() throws Exception {
        Connection conn = DriverManager.getConnection(url, props);

        PreparedStatement st = conn.prepareStatement("SELECT * FROM get_schedule(?)");
        st.setInt(1, department);
        ResultSet rs = st.executeQuery();

        while (rs.next())
        {
            Post post = new Post(rs.getString(1), rs.getInt(2), rs.getInt(3), rs.getInt(4));
            queryResult.add(post);
        }
        rs.close();
        st.close();

        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        output.setPostData(queryResult);
    }
}
