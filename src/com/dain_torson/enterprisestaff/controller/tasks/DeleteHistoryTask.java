package com.dain_torson.enterprisestaff.controller.tasks;

import java.sql.*;
import java.util.Properties;

/**
 * Created by Ales on 10.12.2015.
 */
public class DeleteHistoryTask extends DBTask {

    private String name;
    private String date;

    public DeleteHistoryTask(String url, Properties props, String name, String date) {
        super(url, props);
        this.name = name;
        this.date = date;
    }

    @Override
    protected Void call() throws Exception {

        Connection conn = DriverManager.getConnection(url, props);

        PreparedStatement st = conn.prepareStatement("SELECT * FROM delete_hist(?, ?)");
        st.setString(1, name);
        st.setDate(2, Date.valueOf(date));
        ResultSet rs = st.executeQuery();

        return null;
    }
}
