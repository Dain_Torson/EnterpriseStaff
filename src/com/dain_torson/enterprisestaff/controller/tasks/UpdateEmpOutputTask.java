package com.dain_torson.enterprisestaff.controller.tasks;

import com.dain_torson.enterprisestaff.data.Person;
import com.dain_torson.enterprisestaff.view.DBView;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class UpdateEmpOutputTask extends DBTask{

    private DBView output;
    private List<Person> queryResult;
    private int age;
    private String post;

    public UpdateEmpOutputTask(String url, Properties props, int age, String post, DBView output) {
        super(url, props);
        this.output = output;
        this.age = age;
        this.post = post;
        queryResult = new ArrayList<>();
    }

    @Override
    protected Void call() throws Exception {

        Connection conn = DriverManager.getConnection(url, props);

        PreparedStatement st = conn.prepareStatement("SELECT * FROM get_employes(?, ?)");
        st.setInt(1, age);
        st.setString(2, post);
        ResultSet rs = st.executeQuery();

        while (rs.next())
        {
            Person person = new Person(rs.getString(2), rs.getString(3), rs.getInt(4),
                    rs.getInt(5), rs.getString(6), rs.getString(7));
            queryResult.add(person);
        }
        rs.close();
        st.close();

        return null;
    }

    @Override
    protected void succeeded() {
        super.succeeded();
        output.setPersonData(queryResult);
    }
}
