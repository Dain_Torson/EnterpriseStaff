package com.dain_torson.enterprisestaff.controller;

import com.dain_torson.enterprisestaff.data.EmployeeHistory;
import com.dain_torson.enterprisestaff.data.Person;
import com.dain_torson.enterprisestaff.view.TablePane;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.ArrayList;
import java.util.List;


public class Controller {

    private static final String user = "postgres";
    private static final String password = "postgres";
    private static final String url = "jdbc:postgresql://localhost/mydb";

    private static final String INVALID_VALUE = "Неверное значение";

    @FXML
    private ChoiceBox funcChoiceBox;

    @FXML
    private Label addParams1Label;

    @FXML
    private Label addParams2Label;

    @FXML
    private TextField addParams1Field;

    @FXML
    private TextField addParams2Field;

    @FXML
    private TablePane resultTable;

    @FXML
    private TextField fioEditField;

    @FXML
    private TextField ageEditField;

    @FXML
    private TextField pEditField;

    @FXML
    private TextField depEditField;

    @FXML
    private TextField fioHistoryEditField;

    @FXML
    private TextField departmentEditField;

    @FXML
    private TextField postEditField;

    @FXML
    private TextField fioField;

    @FXML
    private TextField ageField;

    @FXML
    private TextField pField;

    @FXML
    private TextField depField;

    @FXML
    private TextField levelEditField;

    @FXML
    private TextField fioDeleteField;

    @FXML
    private TextField fioHistoryAddField;

    @FXML
    private TextField departmentField;

    @FXML
    private TextField postField;

    @FXML
    private TextField levelField;

    @FXML
    private TextField fioHistoryDeleteField;

    @FXML
    private DatePicker startEditPicker;

    @FXML
    private DatePicker startDeletePicker;

    @FXML
    private DatePicker endEditPicker;

    @FXML
    private DatePicker startPicker;

    @FXML
    private DatePicker endPicker;

    @FXML
    private ChoiceBox<String> genderBox;

    @FXML
    private ChoiceBox<String> maritalStatusBox;

    @FXML
    private ChoiceBox<String> genderEditBox;

    @FXML
    private ChoiceBox<String> maritalStatusEditBox;

    @FXML
    private Button viewResultsButton;

    @FXML
    private Button historySearchButton;

    @FXML
    private Button personSearchButton;

    @FXML
    private Button processEmpButton;

    @FXML
    private Button processHistButton;

    @FXML
    private ToggleGroup actionGroup;

    @FXML
    private ToggleGroup actionGroupHistory;

    private DBController dbController;

    private DBDateConverter converter;

    @FXML
    private void initialize() {

        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Driver loading success!");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        dbController = new DBController(url, user, password, resultTable);

        converter = new DBDateConverter();

        startEditPicker.setConverter(converter);
        endEditPicker.setConverter(converter);
        startPicker.setConverter(converter);
        endPicker.setConverter(converter);
        startDeletePicker.setConverter(converter);

        funcChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new FuncChoiceBoxListener());
        viewResultsButton.setOnAction(new ViewResultsButtonHandler());
        personSearchButton.setOnAction(new PersonSearchButtonHandler());
        historySearchButton.setOnAction(new HistorySearchButtonHandler());
        processEmpButton.setOnAction(new ProcessEmpButtonHandler());
        processHistButton.setOnAction(new ProcessHistButtonHandler());
    }

    private void addEmployee() {
        String fio = fioField.getText();
        String age = ageField.getText();
        int ageInt = 0;

        try {
            ageInt = Integer.valueOf(age);
        }
        catch (NumberFormatException ex) {
            ageField.setText(INVALID_VALUE);
            return;
        }

        String post = pField.getText();
        String dep = depField.getText();
        int depInt = 0;

        try {
            depInt = Integer.valueOf(dep);
        }
        catch (NumberFormatException ex) {
            depField.setText(INVALID_VALUE);
            return;
        }

        String gender = genderBox.getValue().toLowerCase();
        String status = maritalStatusBox.getValue();
        String [] parse = status.split("/");
        if(gender.contains("муж")){
            status = parse[0];
        }
        else {
            status = parse[1];
        }

        Person person = new Person(fio, post, depInt, ageInt, gender, status);
        dbController.addEmployee(person);
    }

    private void updateEmployee() {
        String fio = fioEditField.getText();
        String age = ageEditField.getText();
        int ageInt = 0;

        try {
            ageInt = Integer.valueOf(age);
        }
        catch (NumberFormatException ex) {
            ageEditField.setText(INVALID_VALUE);
        }

        String post = pEditField.getText();
        String dep = depEditField.getText();
        int depInt = 0;

        try {
            depInt = Integer.valueOf(dep);
        }
        catch (NumberFormatException ex) {
            depEditField.setText(INVALID_VALUE);
        }

        String gender = genderEditBox.getValue().toLowerCase();
        String status = maritalStatusEditBox.getValue();
        String [] parse = status.split("/");
        if(gender.contains("муж")){
            status = parse[0];
        }
        else {
            status = parse[1];
        }

        Person person = new Person(fio, post, depInt, ageInt, gender, status);
        dbController.updateEmployee(person);
    }

    private void deleteEmployee(){
        String fio = fioDeleteField.getText();
        dbController.deleteEmployee(fio);
    }

    private void addHistory() {

        String department = departmentField.getText();
        int depInt = 0;
        try {
            depInt = Integer.valueOf(department);
        }
        catch (NumberFormatException ex) {
            departmentField.setText(INVALID_VALUE);
        }

        String post = postField.getText();
        String level = levelField.getText();
        int lvlInt = 0;
        try {
            lvlInt = Integer.valueOf(level);
        }
        catch (NumberFormatException ex) {
            levelField.setText(INVALID_VALUE);
        }

        String startDate = converter.toString(startPicker.getValue());
        String endDate = converter.toString(endPicker.getValue());
        String name = fioHistoryAddField.getText();

        EmployeeHistory history = new EmployeeHistory(depInt, post, lvlInt, startDate, endDate);
        dbController.addHistory(name, history);
    }

    private void updateHistory() {

        String department = departmentEditField.getText();
        int depInt = 0;
        try {
            depInt = Integer.valueOf(department);
        }
        catch (NumberFormatException ex) {
            departmentEditField.setText(INVALID_VALUE);
        }

        String post = postEditField.getText();
        String level = levelEditField.getText();
        int lvlInt = 0;
        try {
            lvlInt = Integer.valueOf(level);
        }
        catch (NumberFormatException ex) {
            levelEditField.setText(INVALID_VALUE);
        }

        String startDate = converter.toString(startEditPicker.getValue());
        String endDate = converter.toString(endEditPicker.getValue());
        String name = fioHistoryEditField.getText();

        EmployeeHistory history = new EmployeeHistory(depInt, post, lvlInt, startDate, endDate);
        dbController.updateHistory(name, history);

    }

    private void deleteHistory() {
        String name = fioHistoryDeleteField.getText();
        String date = converter.toString(startDeletePicker.getValue());

        dbController.deleteHistory(name, date);
    }

    private class FuncChoiceBoxListener implements ChangeListener<Number> {

        @Override
        public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {

            switch (newValue.intValue()) {
                case 0:
                    addParams1Label.setText("Подразделение");
                    addParams1Label.setVisible(true);
                    addParams1Field.setVisible(true);

                    addParams2Label.setVisible(false);
                    addParams2Field.setVisible(false);

                    break;
                case 1:
                    addParams1Label.setVisible(false);
                    addParams1Field.setVisible(false);
                    addParams2Label.setVisible(false);
                    addParams2Field.setVisible(false);
                    break;
                case 2:
                    addParams1Label.setText("Возраст");
                    addParams1Label.setVisible(true);
                    addParams1Field.setVisible(true);

                    addParams2Label.setText("Должность");
                    addParams2Label.setVisible(true);
                    addParams2Field.setVisible(true);
            }
        }
    }

    private class ViewResultsButtonHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {

            int selectedAction = funcChoiceBox.getSelectionModel().getSelectedIndex();

            switch (selectedAction) {

                case 0:
                    String value = addParams1Field.getText();
                    int intValue = 0;
                    try {
                        intValue = Integer.valueOf(value);
                    }
                    catch (NumberFormatException ex) {
                        addParams1Field.setText(INVALID_VALUE);
                        return;
                    }

                    if(intValue < 1 || intValue > 3) {
                        addParams1Field.setText(INVALID_VALUE);
                        return;
                    }

                    dbController.updateSchedule(intValue);
                    break;
                case 1:
                    dbController.updateElders();
                    break;
                case 2:
                    String age = addParams1Field.getText();
                    String post = addParams2Field.getText();

                    int ageInt = 0;
                    try {
                        ageInt = Integer.valueOf(age);
                    }
                    catch (NumberFormatException ex) {
                        addParams1Field.setText(INVALID_VALUE);
                        return;
                    }

                    dbController.updateEmployees(ageInt, post);
                    break;
                default:
            }
        }
    }

    private class PersonSearchButtonHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {

            List<TextField> textFields = new ArrayList<>();
            List<ChoiceBox<String>> choiceBoxes = new ArrayList<>();

            textFields.add(ageEditField);
            textFields.add(pEditField);
            textFields.add(depEditField);

            choiceBoxes.add(genderEditBox);
            choiceBoxes.add(maritalStatusEditBox);

            String name = fioEditField.getText();

            dbController.getPerson(name, textFields, choiceBoxes);

        }
    }

    private class HistorySearchButtonHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {

            List<TextField> textFields = new ArrayList<>();

            textFields.add(departmentEditField);
            textFields.add(postEditField);
            textFields.add(levelEditField);

            String name = fioHistoryEditField.getText();
            String date = converter.toString(startEditPicker.getValue());

            dbController.getHistory(name, date, textFields, endEditPicker);
        }
    }

    private class ProcessEmpButtonHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {

            RadioButton button = (RadioButton) actionGroup.getSelectedToggle();
            String command = button.getText();
            System.out.println(command);

            if(command.contains("Добавить")) {
                addEmployee();
            }
            else if(command.contains("Обновить")) {
                updateEmployee();
            }
            else{
                deleteEmployee();
            }
        }
    }

    private class ProcessHistButtonHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {

            RadioButton button = (RadioButton) actionGroupHistory.getSelectedToggle();
            String command = button.getText();
            System.out.println(command);

            if(command.contains("Добавить")) {
                addHistory();
            }
            else if(command.contains("Обновить")) {
                updateHistory();
            }
            else{
                deleteHistory();
            }

        }
    }

}


