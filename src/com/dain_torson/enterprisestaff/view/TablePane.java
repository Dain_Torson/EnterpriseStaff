package com.dain_torson.enterprisestaff.view;

import com.dain_torson.enterprisestaff.data.Person;
import com.dain_torson.enterprisestaff.data.Post;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

import java.util.List;


public class TablePane extends Pane implements DBView {

    private TableView<Person> personTable;
    private TableView<Post> postTable;

    public TablePane() {
        initTables();
        getChildren().add(postTable);
    }

    private void initTables() {

        personTable = new TableView<Person>();
        postTable = new TableView<Post>();

        TableColumn<Person, String> fioColumn = new TableColumn<Person, String>();
        fioColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("fio"));
        fioColumn.setText("ФИО");
        fioColumn.setPrefWidth(133);

        TableColumn<Person, Integer> ageColumn= new TableColumn<Person, Integer>();
        ageColumn.setCellValueFactory(new PropertyValueFactory<Person, Integer>("age"));
        ageColumn.setPrefWidth(133);
        ageColumn.setText("Возраст");

        TableColumn<Person, String> genderColumn = new TableColumn<Person, String>();
        genderColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("gender"));
        genderColumn.setPrefWidth(133);
        genderColumn.setText("Пол");

        TableColumn<Person, String> maritalStatusColumn = new TableColumn<Person, String>();
        maritalStatusColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("maritalStatus"));
        maritalStatusColumn.setPrefWidth(133);
        maritalStatusColumn.setText("Семейное пол.");

        TableColumn<Person, String> postColumn = new TableColumn<Person, String>();
        postColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("post"));
        postColumn.setPrefWidth(133);
        postColumn.setText("Должность");

        TableColumn<Person, Integer> departmentColumn = new TableColumn<Person, Integer>();
        departmentColumn.setCellValueFactory(new PropertyValueFactory<Person, Integer>("department"));
        departmentColumn.setPrefWidth(133);
        departmentColumn.setText("Подразделение");

        personTable.getColumns().addAll(fioColumn, postColumn, departmentColumn, ageColumn,
                genderColumn, maritalStatusColumn);

        TableColumn<Post, String> nameColumn = new TableColumn<Post, String>();
        nameColumn.setCellValueFactory(new PropertyValueFactory<Post, String>("name"));
        nameColumn.setPrefWidth(200);
        nameColumn.setText("Название");

        TableColumn<Post, Integer> lowerLevelColumn= new TableColumn<Post, Integer>();
        lowerLevelColumn.setCellValueFactory(new PropertyValueFactory<Post, Integer>("lowerLevel"));
        lowerLevelColumn.setPrefWidth(200);
        lowerLevelColumn.setText("Нижняя граница");

        TableColumn<Post, Integer> upperLevelColumn= new TableColumn<Post, Integer>();
        upperLevelColumn.setCellValueFactory(new PropertyValueFactory<Post, Integer>("upperLevel"));
        upperLevelColumn.setPrefWidth(200);
        upperLevelColumn.setText("Верхняя граница");

        TableColumn<Post, Integer> numberColumn= new TableColumn<Post, Integer>();
        numberColumn.setCellValueFactory(new PropertyValueFactory<Post, Integer>("number"));
        numberColumn.setPrefWidth(200);
        numberColumn.setText("Количество");

        postTable.getColumns().addAll(nameColumn, lowerLevelColumn, upperLevelColumn, numberColumn);
    }

    @Override
    public void setPersonData(List<Person> persons) {
        ObservableList<Person> personsList = FXCollections.observableArrayList(persons);
        getChildren().clear();
        getChildren().add(personTable);
        personTable.setItems(personsList);
    }

    @Override
    public void setPostData(List<Post> posts) {
        ObservableList<Post> postList = FXCollections.observableArrayList(posts);
        getChildren().clear();
        getChildren().add(postTable);
        postTable.setItems(postList);
    }
}
