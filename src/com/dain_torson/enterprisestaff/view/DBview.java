package com.dain_torson.enterprisestaff.view;

import com.dain_torson.enterprisestaff.data.Person;
import com.dain_torson.enterprisestaff.data.Post;

import java.util.List;

public interface DBView {
    void setPersonData(List<Person> persons);
    void setPostData(List<Post> posts);
}
